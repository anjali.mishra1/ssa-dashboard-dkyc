import pandas as pd
import psycopg2


print('Connecting to the PostgreSQL database...')

query = "select xcall_id, path, user_id,response_code,properties, properties->>'storeId' as storeId , properties->>'agent_id' as agent_id, properties->>'is_live' as is_live ,  properties->>'capture_type' as capture_type, created_at, issue_curations,issue_curations->>'properphoto' as properphoto  from monitoring_transactions where path = '/v1/check_liveness' and created_at :: DATE = CURRENT_DATE:: DATE-INTERVAL '1 day'";

conn = psycopg2.connect(
user="postgres@jiovishwam-production-curation-db",
password="ilvfjWPZz4pGI7jfvr",
host="10.79.0.62",
port="5432",
database="turing-db")


df=pd.read_sql(sql=query, con = conn)

print('PostgreSQL database version:')


df['date']= pd.to_datetime(df['created_at']).dt.date


flagged_stores=pd.read_csv('/home/vishwam/flagged_stores.csv')

#print(df.columns)
#print(flagged_stores)



def type_data(row):
    result=''
    if (row['capture_type']=='0'):
        result= 'customer'
    elif (row['capture_type']=='1'):
        result= 'agent'
    return result

def liveness_failed(df1):
    
    result=''

    if (df1['is_live']=='yes') :
        result='passed'
    elif (((df1['is_live']=='no') | ((df1['is_live']=="") & (df1['response_code']==422)))):
        result='failed'
    
    return result


def prediction_data(data):
    
    result=''

    if (((data['properphoto']!='false') & (data['response_code']==200)) | ((data['properphoto']=='false') & (data['response_code']==422))):
        result='YES'
    elif (((data['properphoto']=='true') & (data['response_code']==422)) | ((data['properphoto']=='false') & (data['response_code']==200))):
        result='NO'
    
    #result='check'
    return result
    
def clean_storeid(storeid):
    storeid = str(storeid)
    if len(storeid)<10:
        return storeid
    else:
        if(storeid[:1]=='0'):
            return storeid[1:]
        else:
            return storeid


df['type'] = df.apply(lambda df : type_data(df), axis=1) 
df['liveness_failed'] = df.apply(lambda df : liveness_failed(df), axis=1) 
df['correct_response'] = df.apply(lambda df : prediction_data(df), axis=1) 

df['storeid']=df['storeid'].apply(lambda x:clean_storeid(x))
df=df.assign(flagged=df.storeid.isin(flagged_stores['Store Blacklisted']))

df=df.drop(columns=['path','properties', 'is_live', 'capture_type', 'issue_curations','properphoto'])
#df.to_csv('/home/anjali/Downloads/BI_liveness.csv')
print(df)




