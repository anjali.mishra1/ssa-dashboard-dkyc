import pandas as pd
import psycopg2


print('Connecting to the PostgreSQL database...')

query = "select xcall_id, path, user_id,response_code, properties->>'store_id' as store_id , properties->>'journey' as journey, properties->>'is_back' as is_back, created_at  from monitoring_transactions limit 10000";

conn = psycopg2.connect(
user="postgres@jiovishwam-production-curation-db",
password="ilvfjWPZz4pGI7jfvr",
host="10.79.0.62",
port="5432",
database="turing-db")


df=pd.read_sql(sql=query, con = conn)

print('PostgreSQL database version:')


df['date']= pd.to_datetime(df['created_at']).dt.date
print(df.journey.unique())

temp=pd.DataFrame()
def response_count(prod_df):
    for date in prod_df.date.unique():
        df1=prod_df.loc[(prod_df.date == date)]
        de_df= df1[(df1['journey']=='na')]
        qr_df= df1[(df1['journey']!='na')]
        eadhar_df= df1[(df1['journey']=='ea')]
        dl_df= df1[(df1['is_back']==50) | (df1['is_back']==51)]
        pan_df= df1[(df1['is_back']==20)]
        pass_df= df1[(df1['is_back']==30) | (df1['is_back']==31)]
        voter_df= df1[(df1['is_back']==40) | (df1['is_back']==41)]
	#print(prod_df)

        if len(de_df)!=0:
            Orn_count = de_df['user_id'].unique().size
            stores_count = de_df['store_id'].unique().size
        
            response_dict = {'Card Type': 'nonAdhaar','ORN':Orn_count, 'Stores Count' : stores_count}
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            global temp
            temp = pd.concat([temp,dd], axis =1)


#Calculation for qr journey data
        if len(qr_df)!=0:

            Orn_count = qr_df['user_id'].unique().size
            stores_count = qr_df['store_id'].unique().size
        
            response_dict = {'Card Type': 'Adhaar QR','ORN':Orn_count, 'Stores Count' : stores_count}
        
    
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
        
            temp = pd.concat([temp,dd], axis =1)

#Calculation for qr Passport
        if len(pass_df)!=0:

            Orn_count = pass_df['user_id'].unique().size
            stores_count = pass_df['store_id'].unique().size
        
            response_dict = {'Card Type': 'Passport','ORN':Orn_count, 'Stores Count' : stores_count}
        
    
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
        
            temp = pd.concat([temp,dd], axis =1)



#Calculation for Voter
        if len(voter_df)!=0:

            Orn_count = voter_df['user_id'].unique().size
            stores_count = voter_df['store_id'].unique().size
        
            response_dict = {'Card Type': 'Voter','ORN':Orn_count, 'Stores Count' : stores_count}
        
    
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            
            temp = pd.concat([temp,dd], axis =1)



#Calculation for E-Adhaar
        if len(eadhar_df)!=0:

            Orn_count = eadhar_df['user_id'].unique().size
            stores_count = eadhar_df['store_id'].unique().size
        
            response_dict = {'Card Type': 'eAdhaar','ORN':Orn_count, 'Stores Count' : stores_count}
        
    
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            
            temp = pd.concat([temp,dd], axis =1)
            
#Calculation for DRiving License
        if len(dl_df)!=0:

            Orn_count = dl_df['user_id'].unique().size
            stores_count = dl_df['store_id'].unique().size
        
            response_dict = {'Card Type': 'Driving license','ORN':Orn_count, 'Stores Count' : stores_count}
        
    
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            
            temp = pd.concat([temp,dd], axis =1)
            
#Calculation for Pan Card
        if len(pan_df)!=0:

            Orn_count = pan_df['user_id'].unique().size
            stores_count = pan_df['store_id'].unique().size
        
            response_dict = {'Card Type': 'Pan','ORN':Orn_count, 'Stores Count' : stores_count}
        
    
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            
            temp = pd.concat([temp,dd], axis =1)

    return temp




Final_result1 = response_count(df).T
print(Final_result1)
