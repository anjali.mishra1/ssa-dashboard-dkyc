import pandas as pd
import psycopg2


print('Connecting to the PostgreSQL database...')

query = "select xcall_id, path, user_id,response_code, properties->>'store_id' as store_id , properties->>'journey' as journey , properties->>'is_back' as is_back, created_at, issue_curations->>'properAadhaar' as proper_doc  from monitoring_transactions where path = '/v1/read_document' and created_at :: DATE = CURRENT_DATE:: DATE-INTERVAL '1 day'";

conn = psycopg2.connect(
user="postgres@jiovishwam-production-curation-db",
password="ilvfjWPZz4pGI7jfvr",
host="10.79.0.62",
port="5432",
database="turing-db")


df=pd.read_sql(sql=query, con = conn)

print('PostgreSQL database version:')


df['date']= pd.to_datetime(df['created_at']).dt.date
print(df.is_back.unique())

def type_data(row):
    result=''

    if ((row['is_back'].endswith('0')) | (row['is_back'].endswith('3'))):
        result='POI'
    elif (((row['is_back'].endswith('1')) | (row['is_back'].endswith('4'))) & (row['is_back']!='11')):
        result = 'POA'
    elif (row['is_back']=='11'):
        result= 'A4'
    elif (row['is_back']=='12'):
        result= 'Combined'
    return result

def card_data(df1):
    
    result=''

    if (((df1['journey']=='na') & ((df1['is_back']=='0')| (df1['is_back']=='1')))):
        result='Adhaar DE'
    elif (((df1['journey']!='na') & ((df1['is_back']=='0')| (df1['is_back']=='1')))):
        result='Adhaar QR'
    elif ((df1['is_back']=='30')| (df1['is_back']=='31')):
        result= 'Passport'
    elif ((df1['is_back']=='40')| (df1['is_back']=='41')):
        result= 'VoterID'
    elif ((df1['is_back']=='50')| (df1['is_back']=='51')):
        result= 'DrivingLicense'
    elif (df1['is_back']=='20'):
        result= 'Pan'
    return result


def prediction_data(data):
    
    result=''

    if (((data['proper_doc']!='false') & (data['response_code']==200)) | ((data['proper_doc']=='false') & (data['response_code']==422))):
        result='YES'
    elif (((data['proper_doc']=='true') & (data['response_code']==422)) | ((data['proper_doc']=='false') & (data['response_code']==200))):
        result='NO'
    
    #result='check'
    return result


df['type'] = df.apply(lambda df : type_data(df), axis=1) 
df['card'] = df.apply(lambda df : card_data(df), axis=1) 
df['correct_response'] = df.apply(lambda df : prediction_data(df), axis=1) 

df=df.drop(columns=['path', 'journey' , 'is_back' , 'proper_doc'])
df.to_csv('/home/vishwam/dummy_BI_read_doc.csv')
print(df['xcall_id'][(df['correct_response']=='NO')])




