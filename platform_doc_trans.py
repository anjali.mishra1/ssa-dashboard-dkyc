import pandas as pd
import psycopg2


print('Connecting to the PostgreSQL database...')

query = "select xcall_id, req_url, orn,response_code, latency, store_id, properties->>'is_back' as is_back, properties->>'journey' as journey, created_at from transactions limit 1000";

conn = psycopg2.connect(
user="postges@jiovishwam-production-ephemeral-1-dupe",
password="Q4fu5OK6gNOf90D8yC",
host="10.79.0.23",
port="5432",
database="short_term_database")


df=pd.read_sql(sql=query, con = conn)

print('PostgreSQL database version:')


df['date']= pd.to_datetime(df['created_at']).dt.date
print(df.is_back.unique())

#Transaction per second
tps=pd.DataFrame()
def tps_count(df):
    df['created_at'] = pd.to_datetime(df['created_at'], utc=True)
    df['created_at'] = df['created_at'].dt.tz_convert('Asia/Kolkata')

    df['hour'] = df['created_at'].dt.hour
    for hour in df.hour.unique():
        #print(hour)
        tps_hr = len(df[df.hour==hour])/3600   
        response_dict = {'tps_hr':tps_hr}
        hour=hour
        tp = pd.DataFrame.from_dict(response_dict,orient='index',columns = [hour])
        global tps
        tps = pd.concat([tps,tp], axis =1)
    tps=tps.T
    #print(tps)
    return round((tps.tps_hr.mean()),8)



temp=pd.DataFrame()
def response_count(prod_df):
    for date in prod_df.date.unique():
        df1=prod_df.loc[(prod_df.date == date)]
        de_df= df1[(df1['journey']=='na')]
        qr_df= df1[(df1['journey']!='na')]
        eadhar_df= df1[(df1['journey']=='eadhaar')]
        dl_df= df1[(df1['is_back']==50) | (df1['is_back']==51)]
        pan_df= df1[(df1['is_back']==20)]
        pass_df= df1[(df1['is_back']==30) | (df1['is_back']==31)]
        voter_df= df1[(df1['is_back']==40) | (df1['is_back']==41)]
	#print(prod_df)

        if len(de_df)!=0:
            Orn_count = de_df['orn'].unique().size
            Total_200 = len(de_df[(de_df['response_code'] == 200) ])
            Total_424 = len(de_df[(de_df['response_code'] == 424) ])
            Total_404 = len(de_df[(de_df['response_code'] == 404) ])
            Total_406 = len(de_df[(de_df['response_code'] == 406) ])
            POI = len(de_df[(de_df['is_back'] == 0) ])
            POI200 = len(de_df[(de_df['is_back'] == 0) & (de_df['response_code'] == 200) ])
            POA = len(de_df[(de_df['is_back'] == 1) ])
            POA200 = len(de_df[(de_df['is_back'] == 1) & (de_df['response_code'] == 200)])        	
            Avg_latency = de_df['latency'].mean()
            Max_latency = de_df['latency'].max()
            TPS = tps_count(de_df)
        
    
            response_dict = {'Card Type' : 'Adhaar DE','TPS':TPS, 'Avg_latency':Avg_latency, 'Total_200':Total_200, 'Total_424' : Total_424, 'Total_404' : Total_404, 'Total_406' : Total_406, 'POI':POI, 'POI200':POI200, 'POA':POA, 'POA200':POA200, 'ORN':Orn_count, 'Max latency' : Max_latency}
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            global temp
            temp = pd.concat([temp,dd], axis =1)


#Calculation for qr journey data
        if len(qr_df)!=0:

            Orn_count = qr_df['orn'].unique().size
            Total_200 = len(qr_df[(qr_df['response_code'] == 200) ])
            Total_424 = len(qr_df[(qr_df['response_code'] == 424) ])
            Total_404 = len(qr_df[(qr_df['response_code'] == 404) ])
            Total_406 = len(qr_df[(qr_df['response_code'] == 406) ])
            POI = len(qr_df[(qr_df['is_back'] == 0) ])
            POI200 = len(qr_df[(qr_df['is_back'] == 0) & (qr_df['response_code'] == 200) ])
            POA = len(qr_df[(qr_df['is_back'] == 1) ])
            POA200 = len(qr_df[(qr_df['is_back'] == 1) & (qr_df['response_code'] == 200)])        	
            Avg_latency = qr_df['latency'].mean()
            Max_latency = qr_df['latency'].max()
            TPS = tps_count(qr_df)
        
    
            response_dict = {'Card Type' : 'Adhaar QR','TPS':TPS, 'Avg_latency':Avg_latency, 'Total_200':Total_200, 'Total_424' : Total_424, 'Total_404' : Total_404, 'Total_406' : Total_406, 'POI':POI, 'POI200':POI200, 'POA':POA, 'POA200':POA200,'ORN':Orn_count, 'Max latency' : Max_latency}
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
        
            temp = pd.concat([temp,dd], axis =1)

#Calculation for qr Passport
        if len(pass_df)!=0:

            Orn_count = pass_df['orn'].unique().size
            Total_200 = len(pass_df[(pass_df['response_code'] == 200) ])
            Total_424 = len(pass_df[(pass_df['response_code'] == 424) ])
            Total_404 = len(pass_df[(pass_df['response_code'] == 404) ])
            Total_406 = len(pass_df[(pass_df['response_code'] == 406) ])
            POI = len(pass_df[(pass_df['is_back'] == 0) ])
            POI200 = len(pass_df[(pass_df['is_back'] == 0) & (pass_df['response_code'] == 200) ])
            POA = len(pass_df[(pass_df['is_back'] == 1) ])
            POA200 = len(pass_df[(pass_df['is_back'] == 1) & (pass_df['response_code'] == 200)])       
            Avg_latency = pass_df['latency'].mean()
            Max_latency = pass_df['latency'].max()
            TPS = tps_count(pass_df)
        
    
            response_dict = {'Card Type' : 'Passport','TPS':TPS, 'Avg_latency':Avg_latency, 'Total_200':Total_200, 'Total_424' : Total_424, 'Total_404' : Total_404, 'Total_406' : Total_406, 'POI':POI, 'POI200':POI200, 'POA':POA, 'POA200':POA200,'ORN':Orn_count, 'Max latency' : Max_latency}
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
        
            temp = pd.concat([temp,dd], axis =1)



#Calculation for Voter
        if len(voter_df)!=0:

            Orn_count = voter_df['orn'].unique().size
            Total_200 = len(voter_df[(voter_df['response_code'] == 200) ])
            Total_424 = len(voter_df[(voter_df['response_code'] == 424) ])
            Total_404 = len(voter_df[(voter_df['response_code'] == 404) ])
            Total_406 = len(voter_df[(voter_df['response_code'] == 406) ])
            POI = len(voter_df[(voter_df['is_back'] == 0) ])
            POI200 = len(voter_df[(voter_df['is_back'] == 0) & (voter_df['response_code'] == 200) ])
            POA = len(voter_df[(voter_df['is_back'] == 1) ])
            POA200 = len(voter_df[(voter_df['is_back'] == 1) & (voter_df['response_code'] == 200)])        	
            Avg_latency = voter_df['latency'].mean()
            Max_latency = voter_df['latency'].max()
            TPS = tps_count(voter_df)
        
    
            response_dict = {'Card Type' : 'Passport','TPS':TPS, 'Avg_latency':Avg_latency, 'Total_200':Total_200, 'Total_424' : Total_424, 'Total_404' : Total_404, 'Total_406' : Total_406, 'POI':POI, 'POI200':POI200, 'POA':POA, 'POA200':POA200,'ORN':Orn_count, 'Max latency' : Max_latency}
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            
            temp = pd.concat([temp,dd], axis =1)

#Calculation for Voter
        if len(voter_df)!=0:

            Orn_count = voter_df['orn'].unique().size
            Total_200 = len(voter_df[(voter_df['response_code'] == 200) ])
            Total_424 = len(voter_df[(voter_df['response_code'] == 424) ])
            Total_404 = len(voter_df[(voter_df['response_code'] == 404) ])
            Total_406 = len(voter_df[(voter_df['response_code'] == 406) ])
            POI = len(voter_df[(voter_df['is_back'] == 0) ])
            POI200 = len(voter_df[(voter_df['is_back'] == 0) & (voter_df['response_code'] == 200) ])
            POA = len(voter_df[(voter_df['is_back'] == 1) ])
            POA200 = len(voter_df[(voter_df['is_back'] == 1) & (voter_df['response_code'] == 200)])        	
            Avg_latency = voter_df['latency'].mean()
            Max_latency = voter_df['latency'].max()
            TPS = tps_count(voter_df)
        
    
            response_dict = {'Card Type' : 'Passport','TPS':TPS, 'Avg_latency':Avg_latency, 'Total_200':Total_200, 'Total_424' : Total_424, 'Total_404' : Total_404, 'Total_406' : Total_406, 'POI':POI, 'POI200':POI200, 'POA':POA, 'POA200':POA200,'ORN':Orn_count, 'Max latency' : Max_latency}
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            
            temp = pd.concat([temp,dd], axis =1)
            

#Calculation for E-Adhaar
        if len(eadhar_df)!=0:

            Orn_count = eadhar_df['orn'].unique().size
            Total_200 = len(eadhar_df[(eadhar_df['response_code'] == 200) ])
            Total_424 = len(eadhar_df[(eadhar_df['response_code'] == 424) ])
            Total_404 = len(voter_df[(eadhar_df['response_code'] == 404) ])
            Total_406 = len(eadhar_df[(eadhar_df['response_code'] == 406) ])
            POI = len(eadhar_df[(eadhar_df['is_back'] == 0) ])
            POI200 = len(eadhar_df[(eadhar_df['is_back'] == 0) & (eadhar_df['response_code'] == 200) ])
            POA = len(eadhar_df[(eadhar_df['is_back'] == 1) ])
            POA200 = len(eadhar_df[(eadhar_df['is_back'] == 1) & (eadhar_df['response_code'] == 200)])    
            Avg_latency = eadhar_df['latency'].mean()
            Max_latency = eadhar_df['latency'].max()
            TPS = tps_count(eadhar_df)
        
    
            response_dict = {'Card Type' : 'E-Adhaar','TPS':TPS, 'Avg_latency':Avg_latency, 'Total_200':Total_200, 'Total_424' : Total_424, 'Total_404' : Total_404, 'Total_406' : Total_406, 'POI':POI, 'POI200':POI200, 'POA':POA, 'POA200':POA200,'ORN':Orn_count, 'Max latency' : Max_latency}
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            
            temp = pd.concat([temp,dd], axis =1)
            
#Calculation for DRiving License
        if len(dl_df)!=0:

            Orn_count = dl_df['orn'].unique().size
            Total_200 = len(dl_df[(dl_df['response_code'] == 200) ])
            Total_424 = len(dl_df[(dl_df['response_code'] == 424) ])
            Total_404 = len(voter_df[(dl_df['response_code'] == 404) ])
            Total_406 = len(dl_df[(dl_df['response_code'] == 406) ])
            POI = len(dl_df[(dl_df['is_back'] == 0) ])
            POI200 = len(dl_df[(dl_df['is_back'] == 0) & (dl_df['response_code'] == 200) ])
            POA = len(dl_df[(dl_df['is_back'] == 1) ])
            POA200 = len(dl_df[(dl_df['is_back'] == 1) & (dl_df['response_code'] == 200)])    
            Avg_latency = dl_df['latency'].mean()
            Max_latency = dl_df['latency'].max()
            TPS = tps_count(dl_df)
        
    
            response_dict = {'Card Type' : 'Driving License','TPS':TPS, 'Avg_latency':Avg_latency, 'Total_200':Total_200, 'Total_424' : Total_424, 'Total_404' : Total_404, 'Total_406' : Total_406, 'POI':POI, 'POI200':POI200, 'POA':POA, 'POA200':POA200,'ORN':Orn_count, 'Max latency' : Max_latency}
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            
            temp = pd.concat([temp,dd], axis =1)
            
#Calculation for Pan Card
        if len(pan_df)!=0:

            Orn_count = pan_df['orn'].unique().size
            Total_200 = len(pan_df[(pan_df['response_code'] == 200) ])
            Total_424 = len(pan_df[(pan_df['response_code'] == 424) ])
            Total_404 = len(voter_df[(pan_df['response_code'] == 404) ])
            Total_406 = len(pan_df[(pan_df['response_code'] == 406) ])
            POI = len(pan_df[(pan_df['is_back'] == 0) ])
            POI200 = len(pan_df[(pan_df['is_back'] == 0) & (pan_df['response_code'] == 200) ])
            POA = len(pan_df[(pan_df['is_back'] == 1) ])
            POA200 = len(pan_df[(pan_df['is_back'] == 1) & (pan_df['response_code'] == 200)])    
            Avg_latency = pan_df['latency'].mean()
            Max_latency = pan_df['latency'].max()
            TPS = tps_count(pan_df)
        
    
            response_dict = {'Card Type' : 'Pan','TPS':TPS, 'Avg_latency':Avg_latency, 'Total_200':Total_200, 'Total_424' : Total_424, 'Total_404' : Total_404, 'Total_406' : Total_406, 'POI':POI, 'POI200':POI200, 'POA':POA, 'POA200':POA200,'ORN':Orn_count, 'Max latency' : Max_latency}
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            
            temp = pd.concat([temp,dd], axis =1)

    return temp


Final_result1 = response_count(df).T
print(Final_result1)
