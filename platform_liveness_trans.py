import pandas as pd
import psycopg2


print('Connecting to the PostgreSQL database...')

query = "select xcall_id, req_url, orn,response_code, latency, store_id, properties->>'capture_type' as capture_type,  created_at from transactions where req_url='/v1/check_liveness' limit 1000";

conn = psycopg2.connect(
user="postges@jiovishwam-production-ephemeral-1-dupe",
password="Q4fu5OK6gNOf90D8yC",
host="10.79.0.23",
port="5432",
database="short_term_database")


df=pd.read_sql(sql=query, con = conn)

print('PostgreSQL database version:')


df['date']= pd.to_datetime(df['created_at']).dt.date
#print(df.capture_type)

temp = pd.DataFrame()
#Transaction per second
tps=pd.DataFrame()
def tps_count(df):
    df['created_at'] = pd.to_datetime(df['created_at'], utc=True)
    df['created_at'] = df['created_at'].dt.tz_convert('Asia/Kolkata')

    df['hour'] = df['created_at'].dt.hour
    for hour in df.hour.unique():
        #print(hour)
        tps_hr = len(df[df.hour==hour])/3600   
        response_dict = {'tps_hr':tps_hr}
        hour=hour
        tp = pd.DataFrame.from_dict(response_dict,orient='index',columns = [hour])
        global tps
        tps = pd.concat([tps,tp], axis =1)
    tps=tps.T
    #print(tps)
    return round((tps.tps_hr.mean()),8)



#temp=pd.DataFrame()
def response_count(prod_df):
    print("len", len(prod_df))
    for date in prod_df.date.unique():
        df1=prod_df.loc[(prod_df.date == date)]
        #print("len333", len(df1))
        agt_df= df1[(df1['capture_type']=='1')]
        cust_df= df1[(df1['capture_type']=='0')]
        #print("len44", len(cust_df))
        #print("len55", len(agt_df))

      #Agent calculation:  
        if len(agt_df)!=0:
            Orn_count = agt_df['orn'].unique().size
            Total_200 = len(agt_df[(agt_df['response_code'] == 200) ])
            Total_424 = len(agt_df[(agt_df['response_code'] == 424) ])
            Total_404 = len(agt_df[(agt_df['response_code'] == 404) ])
            Total_406 = len(agt_df[(agt_df['response_code'] == 406) ])
            Avg_latency = agt_df['latency'].mean()
            Max_latency = agt_df['latency'].max()
            TPS = tps_count(agt_df)
            #print("tps",TPS)
        
        
    
            response_dict = {'Capture Type' : 'Agent','TPS':TPS, 'Avg_latency':Avg_latency, 'Total_200':Total_200, 'Total_424' : Total_424, 'Total_404' : Total_404, 'Total_406' : Total_406, 'ORN':Orn_count, 'Max latency' : Max_latency}
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            global temp
            temp = pd.concat([temp,dd], axis =1)
            
         #Customer calculation:  
        if len(cust_df)!=0:
            Orn_count = cust_df['orn'].unique().size
            Total_200 = len(cust_df[(cust_df['response_code'] == 200) ])
            Total_424 = len(cust_df[(cust_df['response_code'] == 424) ])
            Total_404 = len(cust_df[(cust_df['response_code'] == 404) ])
            Total_406 = len(cust_df[(cust_df['response_code'] == 406) ])
            Avg_latency = cust_df['latency'].mean()
            Max_latency = cust_df['latency'].max()
            TPS = tps_count(cust_df)

        
    
            response_dict = {'Capture Type' : 'Customer','TPS':TPS, 'Avg_latency':Avg_latency, 'Total_200':Total_200, 'Total_424' : Total_424, 'Total_404' : Total_404, 'Total_406' : Total_406, 'ORN':Orn_count, 'Max latency' : Max_latency}
            date = date
            dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
            #global temp
            temp = pd.concat([temp,dd], axis =1)
            
    return temp


Final_result1 = response_count(df).T
print(Final_result1)
