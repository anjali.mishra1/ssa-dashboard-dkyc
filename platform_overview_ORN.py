import pandas as pd
import psycopg2


print('Connecting to the PostgreSQL database...')

query = "select orn,orn_start,orn_end,journey_status,journey_type,card_type from orn_journey_infos limit 1000";

conn = psycopg2.connect(
user="postges@jiovishwam-production-da",
password="WzYYGLA1e9PV682G2f",
host="10.79.0.20",
port="5432",
database="platform_perf_analysis")


df = pd.read_sql(sql=query, con=conn)

#print('PostgreSQL database version:')


df['date']= pd.to_datetime(df['orn_start']).dt.date
#print(df)

#orn metrics calculation

temp1=pd.DataFrame()
def journey_status(prod_df):
    for date in prod_df.date.unique():
        print(date)
        df=prod_df.loc[(prod_df.date == date)]
        Complete = len(df[df.journey_status == 'complete'])
        Incomplete = df.orn.unique().size
        Till_poi = len(df[(df.journey_status == 'Till_POI') ])
        Till_poa = len(df[df.journey_status == 'Till_POA'])
        Till_Customer_Liveness = len(df[df.journey_status == 'Till_Customer_Liveness'])
        Till_Customer_Match = len(df[df.journey_status == 'Till_Customer_Match'])
        Till_Agent_Liveness = len(df[df.journey_status == 'Till_Agent_Liveness'])
        Till_Agent_Match = len(df[df.journey_status == 'Till_Agent_Match'])
    
        response_dict = {'Complete':Complete, 'Till_poi':Till_poi,'Till_poa':Till_poa,
                         'Till_Customer_Liveness':Till_Customer_Liveness,'Till_Customer_Match':Till_Customer_Match,'Till_Agent_Liveness':Till_Agent_Liveness, 'Till_Agent_Match':Till_Agent_Match}
        date = date
        dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
        global temp1
        temp1 = pd.concat([temp1,dd], axis =1)
    return temp1

result2 = journey_status(df).T
print(result2)
