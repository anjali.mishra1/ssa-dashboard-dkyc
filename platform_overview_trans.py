import pandas as pd
import psycopg2


print('Connecting to the PostgreSQL database...')

query = "select xcall_id, req_url, orn, response_code, latency, store_id,  created_at from transactions where created_at :: DATE = CURRENT_DATE:: DATE-INTERVAL '1 day'";

conn = psycopg2.connect(
user="postges@jiovishwam-production-ephemeral-1-dupe",
password="Q4fu5OK6gNOf90D8yC",
host="10.79.0.23",
port="5432",
database="short_term_database")


df=pd.read_sql(sql=query, con = conn)

print('PostgreSQL database version:')


df['date']= pd.to_datetime(df['created_at']).dt.date
#print(df.loc[0,'properties'])

#Transaction per second
tps=pd.DataFrame()
def tps_count(df):
    df['created_at'] = pd.to_datetime(df['created_at'], utc=True)
    df['created_at'] = df['created_at'].dt.tz_convert('Asia/Kolkata')

    df['hour'] = df['created_at'].dt.hour
    for hour in df.hour.unique():
        #print(hour)
        tps_hr = len(df[df.hour==hour])/3600 
        Auth_tps = len(df[(df.hour==hour) &(df.req_url=='/v1/auth_token')])
        Avg_latency_tps = df[df.hour==hour].latency.mean()  
        response_dict = {'tps_hr':tps_hr, 'Auth_tps':Auth_tps , 'Avg_latency_tps': Avg_latency_tps }
        hour=hour
        tp = pd.DataFrame.from_dict(response_dict,orient='index',columns = [hour])
        global tps
        tps = pd.concat([tps,tp], axis =1)
    tps=tps.T
    tps.to_csv('/home/vishwam/DKYC_TPS.csv')
    #print(tps)
    return round((tps.tps_hr.mean()),8)




temp=pd.DataFrame()
def response_count(prod_df):
    for date in prod_df.date.unique():
        df1=prod_df.loc[(prod_df.date == date)]
        Total_trans = len(df1)
        Orn_count = df1.orn.unique().size
        Total_200 = len(df1[(df1.response_code == 200) ])
        Auth_trans = len(df1[df1.req_url=='/v1/auth_token'])
        Avg_latency = df1.latency.mean()
        TPS = tps_count(df1)
        
    
        response_dict = {'Total_trans':Total_trans, 'Orn_count':Orn_count,'Total_200':Total_200,
                         'TPS':TPS,'Auth_trans':Auth_trans,'Avg_latency':Avg_latency}
        date = date
        dd = pd.DataFrame.from_dict(response_dict,orient='index',columns = [date])
        global temp
        temp = pd.concat([temp,dd], axis =1)
    return temp


Final_result1 = response_count(df).T
print(Final_result1)
#Final_result1.to_csv('/home/vishwam/DKYC_ph_overview_data.csv')
